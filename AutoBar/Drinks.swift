//
//  Drinks.swift
//  AutoBar
//
//  Created by Edward Alvarado on 1/2/16.
//  Copyright © 2016 Edward Alvarado. All rights reserved.
//

import UIKit

class Drinks: NSObject {
    static func getDrinks(jsonData: NSData) -> NSArray {
        do {
            let jsonDict = try NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments) as! NSDictionary
            let results = jsonDict.objectForKey("result") as! NSArray
            return results
        } catch {
            print("error serializing JSON: \(error)")
        }
        return NSArray()
    }
}
