//
//  IngredientsTableViewController.swift
//  AutoBar
//
//  Created by Edward Alvarado on 1/2/16.
//  Copyright © 2016 Edward Alvarado. All rights reserved.
//

import UIKit

class IngredientsTableViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
    var allIngredients : NSArray = NSArray()
    var ingredients : NSMutableArray = NSMutableArray()
    var selectedIngredients : NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Ingredients"
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let data = defaults.objectForKey("selectedIngredients") {
            let store = NSMutableDictionary(dictionary: data as! NSDictionary)
            selectedIngredients = store;
        }
        
        searchBar.delegate = self
        
        allIngredients = Ingredients.getIngredients()
        ingredients = allIngredients.mutableCopy() as! NSMutableArray
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ingredient_cell", forIndexPath: indexPath)

        let title = cell.viewWithTag(1) as! UILabel
        let subtitle = cell.viewWithTag(2) as! UILabel
        let imageView = cell.viewWithTag(3) as! UIImageView

        let ingredient = (ingredients[indexPath.row] as! NSDictionary)
        let name = ingredient.objectForKey("id") as? String
        title.text = name
        subtitle.text = ingredient.objectForKey("description") as? String
        // Configure the cell...

        let result = selectedIngredients.objectForKey(name!)
        
        if (result != nil) {
            cell.backgroundColor = UIColor.lightGrayColor()
        } else {
            cell.backgroundColor = UIColor.blackColor()
        }
        
        var imageURL = "https://assets.absolutdrinks.com/ingredients/" + (ingredient.objectForKey("id") as! String)
        imageURL += ".jpg"
        imageView.sd_setImageWithURL(NSURL(string:imageURL))
        
        return cell
    }
    
    @IBAction func ingredientsSelected(sender: AnyObject) {

        if (selectedIngredients.allKeys.count == 0) {
            return
        }
        var ingredientList = ""
        var allKeys = selectedIngredients.allKeys;
        for (var i = 0; i < allKeys.count; i++) {
            ingredientList += (selectedIngredients.objectForKey(allKeys[i]) as! NSDictionary).objectForKey("id") as! String
            ingredientList += "\n"
        }
        
        let alertController = UIAlertController(title: "Your Ingredients:", message: ingredientList, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(self.selectedIngredients, forKey: "selectedIngredients")
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("drink_vc") as! DrinksViewController
            vc.selectedIngredients = self.selectedIngredients
            self.showViewController(vc, sender: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (UIAlertAction) -> Void in
            
        }))
        showDetailViewController(alertController, sender: nil)
        
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let ingredient = ingredients.objectAtIndex(indexPath.row) as! NSDictionary
        let name = ingredient.objectForKey("id") as! String
        let result = selectedIngredients.objectForKey(name)
        if (result == nil) {
            selectedIngredients.setObject(ingredient, forKey: name)
        } else {
            selectedIngredients.removeObjectForKey(name)
        }
        tableView.reloadData()
    }

}

extension IngredientsTableViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        print("done")
    }
    func searchBar(searchBar: UISearchBar, var textDidChange searchText: String) {
        searchText = searchText.lowercaseString
        //print("change \(searchText)")
        if (searchText.characters.count == 0) {
            ingredients = allIngredients.mutableCopy() as! NSMutableArray
        } else {
            ingredients = NSMutableArray();

            let regex = ".*"+searchText+".*"
            
            for (_, element) in allIngredients.enumerate() {
                let name = element.objectForKey("id") as? String
                if name!.rangeOfString(regex, options: .RegularExpressionSearch) != nil {
                    //print("Item \(index): \(name)")
                    ingredients.addObject(element)

                }
            }
        }
        tableView.reloadData()
    }
}


