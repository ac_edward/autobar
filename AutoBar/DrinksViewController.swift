//
//  DrinksViewController.swift
//  AutoBar
//
//  Created by Edward Alvarado on 1/2/16.
//  Copyright © 2016 Edward Alvarado. All rights reserved.
//

import UIKit

class DrinksViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedIngredients : NSMutableDictionary = NSMutableDictionary()
    var allDrinks : Array<(String, NSArray)> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Drinks"
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        var ingredientsArray : Array<String> = Array()
        var permutations : Array<Array<String>> = Array()
        var selection = selectedIngredients.allKeys;
        for (var i = 0; i < selection.count; i++) {
            let iid = selectedIngredients.objectForKey(selection[i])!.objectForKey("id") as! String
            ingredientsArray.append(iid)
        }
        
        for (var i = 0; i < ingredientsArray.count; i++) {
            for (var j = i ; j < ingredientsArray.count; j++) {
                if i == j {
                    permutations.append([ingredientsArray[i]])
                } else {
                    permutations.append([ingredientsArray[i], ingredientsArray[j]])
                }
            }
        }
        
        
        for (combination) in permutations {
            var ingredientsSelection = ""
            var header = ""
            
            for (aIngredientId) in combination {
                if ingredientsSelection.characters.count > 0 { ingredientsSelection += "/and/" }
                else { ingredientsSelection += "/" }
                ingredientsSelection += aIngredientId
                if header.characters.count > 0 { header += " & " }
                header += aIngredientId
            }

            let url = NSURL(string:"https://addb.absolutdrinks.com/drinks/with" + ingredientsSelection + "?pageSize=5&apiKey=5a15557daca54d61a17f1ff7da1bd51a")
            print("\(url)")
            
            let request: NSURLRequest = NSURLRequest(URL: url!)
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            
            
            let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                if (error == nil) {
                    let drinks = Drinks.getDrinks(data!)
                    self.allDrinks.append((header, drinks))
                    dispatch_async(dispatch_get_main_queue()) {
                        self.collectionView.reloadData()
                    }
                }
            })
            
            task.resume()
        }
    }
}

extension DrinksViewController : UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let drink = allDrinks[indexPath.section].1[indexPath.row] as! NSDictionary
        let descriptionPlain = drink.objectForKey("descriptionPlain") as! String
        let ingredients      = (drink.objectForKey("ingredients") as! NSArray)
        var ingredients_     = ""
        for (var i = 0; i < ingredients.count; i++) {
            ingredients_ += ((ingredients[i] as! NSDictionary).objectForKey("text") as! String)
            ingredients_ += "\n"
        }
        let story = (drink.objectForKey("story") as! String)
        
        var message = story;
        message += "\n\n"
        message += descriptionPlain
        message += "\n\n"
        message += ingredients_;
        
        let alertController = UIAlertController(title:drink.objectForKey("name") as? String, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) -> Void in }))
        showDetailViewController(alertController, sender: nil)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width: self.view.frame.width, height: 50)
    }

}

extension DrinksViewController : UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return allDrinks.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allDrinks[section].1.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        var cell : UICollectionReusableView? = nil
        //if (kind == UICollectionElementKindSectionHeader) {
            cell = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "header_cell", forIndexPath: indexPath)
        let title = cell!.viewWithTag(1) as! UILabel
            title.text = allDrinks[indexPath.section].0
        //}
        return cell!
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("drink_cell", forIndexPath: indexPath)
        let drink = allDrinks[indexPath.section].1[indexPath.row] as! NSDictionary
        let title = cell.viewWithTag(1) as! UILabel
        let subtitle = cell.viewWithTag(2) as! UILabel
        let imageView = cell.viewWithTag(3) as! UIImageView
        let background = cell.viewWithTag(4) as! UIImageView
        let backgroundOverlay = cell.viewWithTag(5)! as UIView

        
        title.text = drink.objectForKey("name") as? String
        
        let ingredients = (drink.objectForKey("ingredients") as! NSArray)
        var ingredients_ = ""
        for (var i = 0; i < ingredients.count; i++) {
            ingredients_ += ((ingredients[i] as! NSDictionary).objectForKey("text") as! String)
            ingredients_ += "\n"
        }
        
        subtitle.text = ingredients_
        
        var imageURL = "https://assets.absolutdrinks.com/drinks/transparent-background-white/100x160/" + (drink.objectForKey("id") as! String)
        imageURL += ".png"
        imageView.sd_setImageWithURL(NSURL(string:imageURL))
        background.sd_setImageWithURL(NSURL(string: imageURL))
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            if backgroundOverlay.subviews.count == 0 {
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                //always fill the view
                blurEffectView.frame = self.view.bounds
                blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
                backgroundOverlay.backgroundColor = UIColor.clearColor()
                backgroundOverlay.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
            }
        }
        else {
            self.view.backgroundColor = UIColor.blackColor()
        }
               return cell
    }
    
}
